import React, { Component } from 'react'
import { Text, View,Image,TouchableOpacity,FlatList,ScrollView } from 'react-native'
import {Post} from '../../components/Post';
import styles from './styles'
import circleData from '../../dummey/Circle'
import postData from '../../dummey/Post'
import {Circle} from '../../components/Circle';
import {ImagePicker} from '../../components/ImagePicker/index'
export default class Home extends Component {
  state = {
    visible: false,
  };
  renderItem = ({ item }) => {
    return (
        <Circle item={item} />
    )
}

toggleImagePicker = () => {
  this.setState({ visible: !this.state.visible })
}
  render() {
    return (
      
      <View style={styles.container}>
      {/* header */}
      <View style={styles.header}>
      <TouchableOpacity style={styles.cameraView} onPress={() => this.toggleImagePicker()} >
      <Image 
      source={require('../../assets/Icons/camera.png')} 
          style={styles.iconCamera}/>
      </TouchableOpacity>
       <Text style={styles.headerName}>Sarygram</Text>
      </View>
      <ImagePicker
          visible={this.state.visible}
          togglePicker={() => this.toggleImagePicker()}

          />
<ScrollView>

    <View style={{paddingLeft:20,width:'100%'}}>

      <FlatList
      horizontal
      contentContainerStyle={styles.circleList}
      showsHorizontalScrollIndicator={false}
      data={circleData}
    renderItem={({item})=><Circle item={item}style={{marginRight:7}} />}
    keyExtractor={(item, index) => `item--${item.id}--${index}`}
    /> 
     </View>
    {/* Post list */}

         <FlatList
         contentContainerStyle={styles.postlist}
      showsVerticalScrollIndicator={false}
      data={postData}
    renderItem={({item})=><Post item={item} />}
    keyExtractor={(item, index) => `item--${item.id}--${index}`}
    /> 
</ScrollView>
{/* Circle list */}

   
      </View>
    )
  }
}
