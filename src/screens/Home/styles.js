import {StyleSheet}from 'react-native'

 const styles = StyleSheet.create({
     container:
     {
        
         flex:1,
     },
     header:{
         width:"100%",
     height:73,
     alignSelf:'center',
     flexDirection:'row',
     alignItems:'center',
     justifyContent:'center', 
     },
     cameraView:{
     width:47,
     height:32,
     alignSelf:'center',
     alignItems:'center',
     justifyContent:'center',
     backgroundColor:'#8C56C6',
     borderTopRightRadius:16,
     borderBottomRightRadius:16,
     position:'absolute',
     left:0
     },
     headerName:{
   
         fontSize:39,
      fontWeight:'bold'       
     },
     iconCamera:{
         height:13,
         width:15,
     },
     cricleList:{
},
     postlist:{
         marginBottom:5
     }
})

export default styles