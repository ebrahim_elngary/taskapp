import React, { Component } from 'react'
import { Text, View, StyleSheet,Image} from 'react-native'
import { Circle } from '../Circle';

 class Post extends Component {
  render() {
    const {item}=this.props
    return (
     <View style={styles.container}>
      <View style={styles.header}>
       <Circle  style={{flexDirection:'row'}} nameStyle={{fontSize:11,width:50,}} item={item}/>
       <View style={styles.cityTime}>
<Text style={styles.title}>{item.city}</Text>
<Text style={styles.title}>{item.date}</Text>
       </View>
       </View>
       <Image 
        style={styles.image} 
        resizeMode={"contain"}
        source={require('../../assets/Images/image.png')}/>
     </View>
    )
  }
}
export {Post}

const styles=StyleSheet.create({
    container:{
      width:"100%",
      borderWidth:0.5,
      borderColor:'gray',
},
    header:{
      flexDirection:'row',
      padding:5
    },
    image:{
      height:201,
    },
    cityTime:{
      height:50,
      flexDirection:'row',
      marginLeft:10,
      width:'80%',
alignItems:'flex-end',
      justifyContent:'space-between'
    },
    title:{
      fontSize:8
    }
})