import React, { Component } from 'react'
import { Image } from 'react-native'

 const TapIcon =({source,style})=> {
 return(
    
    <Image 
    resizeMode={'contain'}
    style={[{width:25,height:25},style]}
    source={source} 
     />

 )
}

export {TapIcon}