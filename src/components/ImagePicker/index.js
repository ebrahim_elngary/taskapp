import React, { Component } from 'react'
import { View, Image, Text, TouchableOpacity, Dimensions, StyleSheet } from 'react-native';
import Modal from "react-native-modal";
import * as Expo from 'expo';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


const { width, height } = Dimensions.get('window');

const PressedItem = ({ name, onPress }) => {
    return (
        <TouchableOpacity activeOpacity={0.5} >
            <Icon name={name} size={100} onPress={() => onPress()} />
        </TouchableOpacity>
    );
}

class ImagePicker extends Component {



    _toggleModal = () => {
        this.props.togglePicker()
    }

    onPressGallery = async () => {
        let result = await Expo.ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,
            aspect: [4, 3],
            base64 : true,
        });
        this.handlePickedImage(result)


    }

    handlePickedImage = (result) => {
        if (!result.cancelled) {
            this.props.onSelectImage(result);
        }
    }

    onPressCamera = async () => {
        const { status } = await Expo.Permissions.askAsync(Expo.Permissions.CAMERA, Expo.Permissions.CAMERA_ROLL);
        if (status == "granted") {
            let result = await Expo.ImagePicker.launchCameraAsync({
                allowsEditing: true,
                aspect: [4, 3],
                base64 : true
            });
            this.handlePickedImage(result)

        }
    }

    render() {
        return (
            <Modal isVisible={this.props.visible}
                onBackdropPress={this._toggleModal}
                onBackButtonPress={this._toggleModal}
                backdropOpacity={0.5}

            >
                    <View style={styles.content} >
                        <PressedItem name="image" onPress={this.onPressGallery} />
                        <PressedItem name="camera" onPress={this.onPressCamera} />
                    </View>
            </Modal>
        )
    }
}

export { ImagePicker }


const styles = StyleSheet.create({
    content: {
        height: height / 4,
        padding: 10,
        width: "100%",
        backgroundColor: '#BA66D3',
        alignItems: 'center',
        borderRadius: 25,
        justifyContent: 'space-around',
        flexDirection: 'row'
    }
})