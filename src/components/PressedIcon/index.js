import React from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, TouchableOpacity, Image, Text } from 'react-native';

const PressedIcon = ({ onPress, style, iconName, underlayColor, source, imageStyle }) => {

    return (
        <TouchableOpacity
            activeOpacity={0.7}
            underlayColor={underlayColor}
            style={[styles.iconContainer, style]}
            onPress={() => onPress != null ? onPress() : alert("Working Soon")}>

            <View style={styles.iconNameView}>
                <Image source={source} style={[styles.image, imageStyle]} />
            </View>
        </TouchableOpacity>
    )
}

PressedIcon.propTypes = {
    color: PropTypes.string,
    underlayColor: PropTypes.string,
    size: PropTypes.number,
    iconName: PropTypes.string,
    style: PropTypes.any,
    source: PropTypes.any,
}
export { PressedIcon };

const styles = StyleSheet.create({
    iconContainer: {
        width: 38,
        height: 38,
        alignItems: 'center',
        justifyContent: 'center'
    },
    image: {
        height: 25,
        width: 25,
    },
    iconNameView: {
        alignItems: 'center',
        textAlign: 'center',
        justifyContent: 'center'
    },
})