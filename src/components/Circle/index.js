import React,{Component}  from 'react'
import { Text, View,StyleSheet,Image } from 'react-native'

  const Circle  =({item,style,nameStyle,imageStyle,borderStyle})=>{
 
 return(
      <View style={[styles.container,style]}>
      <View style={[styles.border,borderStyle]}>
      <Image 
             style={[styles.image,imageStyle]} 
             resizeMode={"contain"} 
             source={require("../../assets/Images/avatar.png")}/>
      </View>

        <Text style={[styles.name,nameStyle]}>{item.name}</Text>
      </View>
 )
}

export   {Circle}

const styles =StyleSheet.create({
  container:{
    width:54,
  },
  border:{
    width:56,
    height:56,
    borderRadius:27,
    borderWidth:2,
    borderColor:"#8C56C6",
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'#fff'
  },
  image:{
    width:50,
    height:50,
    borderRadius:25,
    
  },
  name:{
    textAlign:'center',
fontSize:7,
margin:7
  },
})