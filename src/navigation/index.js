import React, { Component } from 'react'
import { Text, View ,Image} from 'react-native'

import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import HomeScreen from '../screens/Home'
import SearchScreen from '../screens/Search'
import SettingsScreen from '../screens/Profile'
import ProfileScreen from '../screens/Settings'
import FavoriteScreen from '../screens/Favorite'
import { ICONS } from '../common';
import { TapIcon } from '../components/TapIcon';


const TabNavigator = createBottomTabNavigator({
  Settings:{screen:SettingsScreen,

    navigationOptions: {
      tabBarIcon: () => (
       <TapIcon
       source={ICONS.Settings}
       />
        ),
  }
},
    Search:{screen:SearchScreen,
      navigationOptions: {
  
        tabBarIcon: () => (
         <TapIcon
         source={ICONS.Search}
         />
          ),
    }
    },
    Home: {screen:HomeScreen,
      navigationOptions: {
   

        tabBarIcon: () => (
          <View style={{width:50,height:50,borderRadius:25,backgroundColor:'#8C56C6',alignItems:'center',justifyContent:'center'}}>
  

            <TapIcon
            source={ICONS.Home}
            style={{width:19,height:19}}
            />
          </View>
          ),
    }
    },
    Favorite:{screen:FavoriteScreen,
      navigationOptions: {
        
        tabBarIcon: () => (
          <TapIcon
          source={ICONS.Favorite}
          />
          ),
        }
      },
      Profile:{screen:ProfileScreen,
        navigationOptions: {
     
          tabBarIcon: () => (
           <TapIcon
           source={ICONS.Profile}
           />
            ),
          }
      },
    },
    
    {
      initialRouteName:'Home',
      tabBarOptions:{
        showLabel:false
      },
  });
  

export default createAppContainer(TabNavigator);
